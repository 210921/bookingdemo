package romaniancoder.booking;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
public class ViewController {

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("datetime",new Date());
        model.addAttribute("username","Romanian Coder");
        model.addAttribute("mode","production");
        return "/index";
    }


    @RequestMapping("/hello")
    public String helllo() {
        return "/hello";
    }

}
